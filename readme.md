# vite-react-hooks-manager 项目搭建

React-hooks + router-router-dom + vite

node 版本 v21.2.0 

- 【已完成】 自定义嵌套路由-同步vue-router
- 【已完成】 菜单动态渲染，直接通过router的路径配置菜单
<!-- - 【已完成】 自定义文案国际化，基于react-i18next  暂不使用-->
<!-- - 【未完成】 UI组件语言国际化 -->
<!-- - 【未完成】 UI组件时间国际化 -->
- 【已完成】 全局message函数引入
- 【已完成】 layout 响应式结构布局
- 【已完成】 基本表单实现，antd组件基本用法
- 【已完成】 路由token权限拦截，异常页面重定向跳转功能
- 【已完成】 基于lazy 和suspence处理，类似vue-router的懒加载,修复闪屏问题(提供显示 loading 状态的阈值)
- 【已完成】 基于router的loading自动加载过程, Suspense功能中有个callback函数，可以在加入加载中的页面节点
- 【已完成】 环境变量输出, 在不同环境下打包或者运行时，通过同一个变量名获取不同环境下所需要的变量
- 【已完成】 全局状态存储，刷新， 利用的是hooks + contenxt API
- 【已完成】 父子通信，同vue 的props和emit不同， react中子给父组件传递信息实时直接调用父组件的函数
- 【已完成】 列表渲染, 没什么好说的antd的组件，主要关注的hooks在业务中的生命周期和执行顺序
- 【已完成】 样式预处理sass修改为less, 相关定义变量和引入方式同步调整,便于 antd 样式的同步
- 【已完成】 classNames处理多类样式
- 【已完成】 打包根目录路径测试
- 【已完成】 自定义样式覆盖
- 【已完成】 UI组件多主题色配置
- 【未完成】 keep-alive页面缓存
- 【已完成】 进入弹窗组件前或者退出弹窗组件后，清空表单数据
- 【已完成】 时间国际化
- 【已完成】 支持svg的导入


#### Drawer抽屉组件模板

```js
   const [open, setOpen] = useState(false);
    const showDrawer = () => {
        setOpen(true);
    };

    const onClose = () => {
        setOpen(false);
    };
    
 <Button type="primary" shape="round"  size="large" onClick={showDrawer}>
                    打开抽屉组件
</Button>
  <Drawer title="Basic Drawer" placement="right" onClose={onClose} open={open}>
                <p>抽屉组件.</p>
</Drawer>
```



### 表格列控制显示隐藏用法
```js
import { createColumns } from "./columns"
import { columnsCheck } from "@/components/ColumnsCheck";

const columnsSource =  createColumns();
let [columns, setColumns] = useState<object[]>(columnsSource);

function columnsCheckCb(data: any) {
        setColumns(data.filter((v: any) => {
            return v.show
        }))
    }
```