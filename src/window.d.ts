
import { MessageInstance } from 'antd';

declare module 'lodash'
declare function setTimeout(callback: (...args: any[]) => void, ms: number, ...args: any[]): NodeJS.Timer;

declare global {
    interface Window {
      $message?: MessageInstance;
    }
  }
  