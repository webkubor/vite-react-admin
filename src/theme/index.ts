 

 /**
  * @description: antd全局样式自定义
  * @return {*}
  */ 
 const theme = {
    token: {
      colorBgLayout: '#EDF2F9',
      colorPrimary: '#6A74FC',
      colorSuccess: "#52c41a",
      colorWarning:"#faad14",
      colorError:"#ff4d4f",
      colorInfo:"#1677ff",
      colorTextBase:"#000000",
      fontWeightStrong:600,
      fontSize: 14,
      borderRadius: 6
    },
  };

  export default theme