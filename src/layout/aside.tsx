import { useState } from 'react';
import { menuChildren } from '../router';
import type { MenuProps } from 'antd';
import { Menu, Space } from 'antd';
import { useNavigate } from 'react-router-dom';
import { useSettingContext } from '@/contexts/setting';
import LogoUrl from "@/assets/logo.svg";

function aside() {
    const [router] = useState(menuChildren)
    const navigate = useNavigate()
    const sessionSelectKeys = sessionStorage.getItem("setSelectKeys") || ''
    const sessionOpenKeys = sessionStorage.getItem("setOpenKeys") || '[]'
    const [selectKeys, setSelectKeys] = useState<string>(sessionSelectKeys)
    const [openKeys, setOpenKeys] = useState<[]>(JSON.parse(sessionOpenKeys))
    const { settingState } = useSettingContext();

    const handerMenu: MenuProps['onClick'] = (e) => {
        const keyPath = e.keyPath
        const route = e.keyPath.reverse().join('/')

        console.log('/layout/' + route, keyPath)
        setOpenKeys(keyPath.slice(1))
        navigate('/layout/' + route)
        setSelectKeys(e.key)
        sessionStorage.setItem('setSelectKeys', e.key)
        sessionStorage.setItem('setOpenKeys', JSON.stringify(keyPath.slice(1)))
    };
    return (
        < >
            <div style={{ height: 54, color:'#1890FF',  fontWeight: 600, display: 'flex',justifyContent: "center", marginBottom: 10}} >
                <Space>
                    <img src={LogoUrl} style={{height:30}} alt="avatar" />
                    {
                        !settingState.collapsed && (<span style={{fontSize: 20}}>Manager </span>)
                    }
                    
                </Space>
            </div>
            <Menu
                onClick={handerMenu}
                mode="inline"
                defaultOpenKeys={openKeys}
                defaultSelectedKeys={selectKeys}
                theme="dark"
                items={router}
            />
        </>
    )
}


export default aside