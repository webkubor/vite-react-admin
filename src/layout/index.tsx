import React from 'react';
import { Layout } from 'antd';
import { Outlet } from 'react-router-dom';
import Aside from './aside';
import TopHeader from './header';

import { useSettingContext } from "@/contexts/setting";

const { Header, Sider, Content } = Layout;

const headerStyle: React.CSSProperties = {
  textAlign: 'center',
  color: '#333333',
  height: 64,
  display:'flex',
  justifyContent:"space-between",
  alignItems: "center",
  paddingInline: 20,
  backgroundColor: '#ffffff',
};

const contentStyle: React.CSSProperties = {
  minHeight: '800px',
  margin: '24px 16px 0', 
  padding: "20px",
  color: '#333333',
  backgroundColor: '#ffffff',
  overflow: 'initial'
};


function Main() {
  const { settingState } = useSettingContext();
  return (
    <Layout hasSider   style={{ minHeight: '100vh' }}>
      <Sider collapsed={settingState.collapsed}>
        <Aside />
      </Sider>
      <Layout className="site-layout">
        <Header style={headerStyle}>
          <TopHeader/>
        </Header>
        <Content style={contentStyle}>
          <Outlet />
        </Content>
      </Layout>
    </Layout>
 
  )
}


export default Main;