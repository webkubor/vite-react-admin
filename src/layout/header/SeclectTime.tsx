
import { Popover, Input } from 'antd';
import timezone from 'moment-timezone';
import { useState } from 'react';
import { SearchOutlined } from '@ant-design/icons';
import { useUserContext } from "@/contexts/user";

let timeList = timezone.tz.names();

export default function SeclectTime() {
    const { state, dispatch } = useUserContext();
    let [searchTimeZone, setSearchTimeZone] = useState("");

    // 搜索时区
    function onSearchTimeArea(v: string) {
        setSearchTimeZone(v)
        if (v == "") {
            timeList = timezone.tz.names()
            return;
        };
        let reg = new RegExp(v, "i");
        timeList = timezone.tz.names().filter((item) => {
            return reg.test(item);
        });
    }
    // 选择时区
    function onSelectTime(v: string, dispatch: Function) {
        localStorage.time_zone = v;
        dispatch({ type: "switch_time_zone", payload: v })
    }

    const timeAreaContent = <div className='time-zone-list'>
        <Input value={searchTimeZone} onChange={(e) => { onSearchTimeArea(e.target.value) }} suffix={<SearchOutlined />} />
        {timeList.map((item, index) => {
            return <div className='zone-item' key={index} onClick={() => onSelectTime(item, dispatch)}>
                <span> {item}</span>
                <span className="iconfont icon-success active"></span>
            </div>

        })}
    </div>

    return <Popover placement="bottom" content={timeAreaContent}>
        <div>{state.timeArea}</div>
    </Popover>
}