import React, { useState, useRef } from 'react';
import { Button, Alert } from 'antd';
import GoogleCode from '@/components/GoogleCode';
import Gmodal from '@/components/Gmodal';
import { findDOMNode } from "react-dom";


function BindWarning() {
    const [isModalOpen, setModalShow] = useState<boolean>(false);
    const [saveLoad, setSaveLoad] = useState<boolean>(false);
    const addBtn = useRef(null);

    const setAddBtnblur = () => {
        if (addBtn.current) {
            // eslint-disable-next-line react/no-find-dom-node
            const addBtnDom = findDOMNode(addBtn.current) as HTMLButtonElement;
            setTimeout(() => addBtnDom.blur(), 0);
        }
    };

    const handleDone = () => {
        setAddBtnblur();
        setModalShow(false);
    };

    const handleCancel = () => {
        setAddBtnblur();
        setModalShow(false);
    };

    function getCode(code: string) {
        console.log('%c%s', 'color: #00a3cc', "获取" + code);
    }

    return (
        <React.Fragment>
            <Alert
                message="为了您的账户和资金安全，请尽快绑定谷歌验证码"
                type="warning"
                style={{ height: '40px', width: "500px" }}
                showIcon
                action={
                    <Button size="small" ghost type='primary' className='button-color-yellow' onClick={() => setModalShow(true)}>
                        去绑定
                    </Button>
                }
            />

            <Gmodal title="谷歌验证器" show={isModalOpen} onDone={handleDone} onCancel={handleCancel}>
                <GoogleCode saveLoading={saveLoad} onSave={getCode} onClose={() => { setModalShow(false); }}  />
            </Gmodal>
        </React.Fragment>
    )
}




export default BindWarning