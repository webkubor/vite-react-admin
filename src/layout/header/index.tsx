
import "./header.less";
import { Space, Avatar } from 'antd';
import React from 'react';
import { UserOutlined, MenuUnfoldOutlined, MenuFoldOutlined } from '@ant-design/icons';
import { useSettingContext } from "@/contexts/setting";
import SeclectTime from "./SeclectTime";


function header() {
    const { settingState, setGlobalConfig } = useSettingContext();

    return (
        <React.Fragment>
            <Space align="center">
                <div onClick={() => setGlobalConfig({ type: "toggle_collapsed" })}> {settingState.collapsed ? <MenuUnfoldOutlined style={{ fontSize: '18px' }} />
                    : <MenuFoldOutlined style={{ fontSize: '18px' }} />}
                </div>
            </Space>
            <Space size={15} align="end">
                <SeclectTime />
                <Avatar style={{ backgroundColor: '#87d068' }} icon={<UserOutlined />} />
                webkubor
            </Space>

        </React.Fragment>
    )
}

export default header