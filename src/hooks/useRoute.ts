import React from "react";


export interface  MetaProps {
  keepAlive?:boolean;
  key?:string;
  auth?: number;
}

export interface RouteObject {
    children?: RouteObject[];
    element?: React.ReactNode;
    path?:string;
    meta?: MetaProps;
}


/**
 * @description: 根据 path 获取路由对象详情
 * @param {string} path
 * @param {Object[]} routes
 * @return {RouteObject}
 */
export const matchRoute = (path: string, routes: Object[]): RouteObject => {
  for (const item of routes) {
    if (item.path === path) {
      return item;
    }
    if (item.children && item.children.length) {
      const res = matchRoute(path, item.children);
      if (Object.keys(res).length) {
        return res;
      }
    }
  }
  return {};
};