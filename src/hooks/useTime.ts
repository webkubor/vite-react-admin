
import moment from 'moment'
import timezone from 'moment-timezone'
import {getLangStorage} from "@/utils/storage"

// 检测语言变化，改变时间语言
function getLanguageZh() {
    moment.defineLocale('zh-cn', {
        relativeTime: {
            future: '%s前',

            past: '%s前',

            s: '幾秒',

            m: '1 分鐘',

            mm: '%d 分鐘',

            h: '1 小時',

            hh: '%d 小時',

            d: '1 天',

            dd: '%d 天',

            M: '1 个月',

            MM: '%d 个月',

            y: '1 年',

            yy: '%d 年'
        }
    })
}
// 检测语言变化，改变时间语言
function getLanguageEs() {
    moment.defineLocale('en-cn', {
        relativeTime: {
            future: '%s ago',

            past: '%s ago',

            s: 'a second',

            m: 'a minute',

            mm: '%d minute',

            h: 'a hour',

            hh: '%d hour',

            d: 'a day',

            dd: '%d day',

            M: 'a months',

            MM: '%d months',

            y: 'a year',

            yy: '%d year'
        }
    })
}

/**
 * @description: 获取本地时间
 * @param {*} timestamp 毫秒數
 * @param {*} format 格式
 * @return {*}
 */
export function getLocalTime(timestamp: number, format = 'YYYY-MM-DD HH:mm:ss') {
    return moment(timestamp).format(format)
}

/**
 * @description: 根据时区按照一定规则生成日期
 * @param {*} timestamp 毫秒數
 * @param {*} format 格式
 * @return {*}
 */
export function getUTCTime(timestamp: number, format = 'YYYY-MM-DD HH:mm:ss', timeArea = "UTC") {
    if (!timestamp) {
        return  ''
    }
    if (timeArea) {
        return moment.tz(timestamp, timeArea).format(format)
    }
    return moment.tz(timestamp, timeArea).format(format)
}

/**
 * @description: 获取相对时间
 * @param {*} timestamp 毫秒數
 * @param {*} format 格式
 * @return {*}
 */
export function getRelativeTime(timestamp: number, format = 'day') {
    const timeZone =
        localStorage.time_zone && localStorage.getItem('time_zone')
    const lang = getLangStorage() 
    if (lang == 'zh') {
        getLanguageZh()
    } else {
        getLanguageEs()
    }

    return moment.tz(timestamp, timeZone).fromNow()
}

/**
 * @description: 根据时区获取当前时区的时间戳
 * @param {*} timestamp 毫秒數
 * @param {*} format 格式
 * @return {*}
 */
export function timeZoneGetTime(timestamp: number, timeZone="UTC") {
    const time = moment.tz(timestamp, timeZone).format()
    return Date.parse(time) / 1000
}

/**
 * @description: 获取当前时区
 * @param {*} timestamp 毫秒數
 * @param {*} format 格式
 * @return {*}
 */
export function getTimeZone() {
    return moment.tz?.guess()
}

/**
 * @description: 获取时区列表
 * @param {*}
 * @return {*}
 */
export function getAllTimeZone() {
    return timezone.tz.names()
}

export function useTime() {
    const setDefaultTimeArea = () => {
        moment.tz.setDefault(moment.tz.guess())
        console.log(moment.tz.guess(), 'test')
    }
    return {
        getLocalTime,
        getUTCTime,
        setDefaultTimeArea,
        getRelativeTime,
        timeZoneGetTime,
        getTimeZone
    }
}