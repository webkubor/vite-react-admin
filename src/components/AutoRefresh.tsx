import React, { useEffect, useRef, useState } from 'react';
import { Switch, Space } from 'antd';


function AutoRefresh(props: { onFetchList: Function }) {
    const { onFetchList } = props;
    let [time, setTime] = useState(import.meta.env.VITE_BASE_TIME_DOWN);
    let [checkedValue, setCheckedValue] = useState(true);
    let timer = useRef<any>(null);

    function onChange(checked: boolean) {
        setCheckedValue(checked);
        if (checked) {
            timeDown()
        } else {
            clearInterval(timer.current)
        }
    }

    function timeDown() {
        timer && clearInterval(timer.current)
        timer.current = setInterval(() => {
            setTime(time -= 1)
            if (time < 1) {
                onFetchList();
                setTime(import.meta.env.VITE_BASE_TIME_DOWN)
            }
        }, 1000);
    }

    useEffect(() => {
        timeDown();
    })

    return <>
        <Space>
            自动刷新 {time} s
            <Switch checked={checkedValue} onChange={onChange} />
        </Space>

    </>
}

export default AutoRefresh