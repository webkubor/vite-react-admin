
// 全局模态窗

import { Modal } from 'antd';

interface GmodalType {
    title?: string,
    show: boolean | undefined,
    children?: JSX.Element,
    onCancel: () => void,
    onDone: () => void
}

function Gmodal(props: GmodalType) {
    let { title, show, onCancel, onDone, children } = props;
    const modalFooter = { okText: "保存",cancelText: "取消",  onOk: onDone, onCancel };

    return (
        <Modal title={title} open={show} width={640}  bodyStyle={{ padding: "28px 0 0" }} destroyOnClose  {...modalFooter}>
            {children}
        </Modal>
    )
}


export default Gmodal