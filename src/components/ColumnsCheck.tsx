import { Space, Checkbox } from "antd";
import _ from "lodash"
import { useState } from "react";

function ColumnsCheck({ columnsSource, columnsCheckCb }: {
    columnsSource: unknown[], columnsCheckCb: Function
}) {
    let [data, setData] = useState(_.cloneDeep(columnsSource));

    function onChange(item:any) {
        item.show = !item.show;
        columnsCheckCb(data);
    }
    return <>
        <Space direction="vertical">
            {data.map((item: any, index) => {
                return <Checkbox checked={item.show} key={item.title} onChange={() => onChange(item)} >
                    {typeof item?.title === "string" ? item.title : item?.title()}
                </Checkbox>
            })}
        </Space>
    </>
}

export default ColumnsCheck