import { useRoutes, Navigate, RouteObject } from "react-router-dom";
import { SettingOutlined, DashboardOutlined,MoneyCollectOutlined,TrademarkOutlined, UnorderedListOutlined } from '@ant-design/icons';
import { lazy } from "react";
import lazyLoad from "./layLoad";
import Layout from "@/layout";
import Login  from "@/views/auth/login"


/**
 * @description: 
 * path: 字符串类型，表示路由的路径。
 * label: 字符串类型，表示路由的标签。
 *  key: 字符串类型，表示路由的唯一标识。
 * hidden: 布尔类型，表示路由是否隐藏。
 * element: React.LazyExoticComponent<React.FC> 类型，表示使用 React.lazy() 方法加载的组件。
 * meta: 包含 auth 和 keepAlive 两个属性的对象，auth 属性为数字类型，表示该路由需要的权限等级，keepAlive 属性为布尔类型，表示是否需要缓存该路由。
 */

export const menuChildren:Array<RouteInfo> =[
    {
        path: "*",
        hidden: true, 
        element: <Navigate to="/layout/404" />
    },
    {
        path: "403",
        label: "403",
        key: "403",
        hidden: true,
        element: lazyLoad(lazy(() => import("../views/result/403"))),
        meta: {
            auth: 0,
            keepAlive: false
        }
    },
    {
        path: "404",
        label: "404",
        key: "404",
        hidden: true,
        element: lazyLoad(lazy(() => import("../views/result/404"))),
        meta: {
            auth: 0,
            keepAlive: false
        }
    },
    {
        path: "dashboard",
        key: "dashboard",
        icon: <DashboardOutlined  style={{ fontSize: '18px' }} />,
        label: "首页",
        element:lazyLoad(lazy(() => import("../views/dashboard"))),
        index: + true,  // fixed 初始化后 menu菜单渲染的错误问题
        meta: {
            auth: 1,
            keepAlive: false
        }
    },
    {
        path: "order",
        key: "order",
        icon: <UnorderedListOutlined  style={{ fontSize: '18px' }} />,
        label: "待审核发送",
        element: lazyLoad(lazy(() => import("../views/order/index"))),
        index: + true,  // fixed 初始化后 menu菜单渲染的错误问题
        meta: {
            auth: 1,
            keepAlive: false
        }
    },
   
    {
        path: "trade",
        key: "trade",
        label: "交易记录",
        icon: <TrademarkOutlined   style={{ fontSize: '18px' }}/>,
        element: lazyLoad(lazy(() => import("../views/trade/index"))),
        meta: {
            auth: 1,
            keepAlive: true
        }
    },
    {
        path: "merchant",
        key: "merchant",
        label: "商户管理",
        icon: <MoneyCollectOutlined  style={{ fontSize: '18px' }}/>,
        element: lazyLoad(lazy(() => import("@/views/merchant/index"))),
        meta: {
            auth: 1,
            keepAlive: true
        }
    },
    {
        path: "operation",
        key: "operation",
        label: "运营管理",
        icon: <SettingOutlined   style={{ fontSize: '18px' }} />,
        element: lazyLoad(lazy(() => import("../views/operation/index"))),
        meta: {
            auth: 1,
            keepAlive: true
        }
    },
    {
        path: "finance",
        key: "finance",
        label: "财务管理",
        icon: <SettingOutlined   style={{ fontSize: '18px' }} />,
        element: lazyLoad(lazy(() => import("../views/finance/index"))),
        meta: {
            auth: 1,
            keepAlive: true
        }
    },
    {
        path: "report",
        key: "report",
        label: "报表管理",
        icon: <SettingOutlined   style={{ fontSize: '18px' }} />,
        element: lazyLoad(lazy(() => import("../views/reportForm/index"))),
        meta: {
            auth: 1,
            keepAlive: true
        }
    },
    {
        path: "safety",
        key: "safety",
        label: "安全管理",
        icon: <SettingOutlined   style={{ fontSize: '18px' }} />,
        element: lazyLoad(lazy(() => import("../views/safety/index"))),
        meta: {
            auth: 1,
            keepAlive: true
        }
    },
    {
        path: "setting",
        key: "setting",
        label: "系统管理",
        icon: <SettingOutlined   style={{ fontSize: '18px' }} />,
        element: lazyLoad(lazy(() => import("../views/setting/index"))),
        meta: {
            auth: 1,
            keepAlive: true
        }
    }
]


interface RouteInfo {
    path: string;
    label: string;
    key: string;
    hidden: boolean;
    // element: React.LazyExoticComponent<React.FC<{}>>;
    element: React.FC;
    meta: {
      auth: number;
      keepAlive: boolean;
    };
  }
  

  export const layoutRouter: Array<Object> = [
    {
        path: "/",
        label: "首页",
        key: "index",
        hidden: true,
        element: <Navigate to='/layout' />,
        meta: {
            auth: 0
        }
    },
   
    {
        path: "/login",
        label: "登录",
        key: "login",
        hidden: true,
        element: <Login></Login>,
        meta: {
            auth: 0
        }
    },
    {
        path: '/layout',
        key: "layout",
        element: <Layout></Layout>,
        meta: {
            auth: 0
        },
        children:menuChildren
    }
]




const GetRouters = () => {
    return useRoutes(layoutRouter)
}
export default GetRouters;