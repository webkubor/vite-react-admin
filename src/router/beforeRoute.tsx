import { Navigate, useLocation } from "react-router-dom"
import { layoutRouter } from "./index"
import { matchRoute } from "@/hooks/useRoute"

function BeforeRouter(props: { children: JSX.Element }) {
    const token = localStorage.getItem('token')

    const { pathname } = useLocation()

    const router =  matchRoute(pathname, layoutRouter)

    if (!router.meta?.auth) {
        return props.children 
    } 

    if (!token) {
        return <Navigate to="/login" />
    } else {
    console.log(router, "当前路由配置信息");
        return props.children
    }
}


export default BeforeRouter 