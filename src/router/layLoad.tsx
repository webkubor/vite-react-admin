import Loader from "@/components/Loader"
import React, { Suspense } from "react"

// Suspense 组件传入了 fallback 属性，这个属性用于显示加载中的页面，就是俗称的 loading 咯。

//如果一个异步请求数据的过程非常快，这样会使得加载中画面一闪而过，导致闪屏。

const lazyLoad = (Comp:React.LazyExoticComponent<()=> JSX.Element>)  => {
    return (
        <Suspense fallback={<Loader/>}>
            <Comp/>
        </Suspense>
    )
}
 


export default lazyLoad