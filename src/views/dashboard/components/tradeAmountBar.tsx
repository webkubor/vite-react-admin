import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Bar } from '@ant-design/plots';

function TradeAmountBar() {

  const data = [
    {
      label: 'Mon.',
      type: '接收',
      value: 2800,
    },
    {
      label: 'Mon.',
      type: '发送',
      value: 2260,
    },
    {
      label: 'Tues.',
      type: '接收',
      value: 1800,
    },
    {
      label: 'Tues.',
      type: '发送',
      value: 1300,
    },
    {
      label: 'Wed.',
      type: '接收',
      value: 950,
    },
    {
      label: 'Wed.',
      type: '发送',
      value: 900,
    },
    {
      label: 'Thur.',
      type: '接收',
      value: 500,
    },
    {
      label: 'Thur.',
      type: '发送',
      value: 390,
    }
  ];
  const config = {
    data,
    isGroup: true,
    xField: 'value',
    yField: 'label',

    /** 自定义颜色 */
    // color: ['#1383ab', '#c52125'],
    seriesField: 'type',
    marginRatio: 0,
    label: {
      // 可手动配置 label 数据标签位置
      position: 'middle',
      // 'left', 'middle', 'right'
      // 可配置附加的布局方法
      layout: [
        // 柱形图数据标签位置自动调整
        {
          type: 'interval-adjust-position',
        }, // 数据标签防遮挡
        {
          type: 'interval-hide-overlap',
        }, // 数据标签文颜色自动调整
        {
          type: 'adjust-color',
        },
      ],
    },
    legend: {
      layout: 'horizontal',
      position: 'top-left'
    },
  }
  return (
    <>
      <Bar {...config} />
    </>
  )
}
export default TradeAmountBar
