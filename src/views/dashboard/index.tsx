import { Card, Tabs, Space, Row, Col } from 'antd'
import type { TabsProps } from 'antd';
import TradeAmountBar from './components/tradeAmountBar';
import TradeNumberBar from './components/tradeNumberBar';
import ReceiveBar from './components/receiveBar';
import ReceivePay from './components/receivePay';
import './dashboard.less'

const onChange = (key: string) => {
  console.log(key);
};

const items: TabsProps['items'] = [
  {
    key: '1',
    label: `交易金额分析`,
    children: <TradeAmountBar />,
  },
  {
    key: '2',
    label: `交易笔数分析`,
    children: <TradeNumberBar />,
  }
];

function Dashboard() {
  // const { t, i18n } = useTranslation();

  return (
    <>
      <Card bordered={false}>
        <Row gutter={[16, 16]}>
          <Col xs={3} sm={5} md={6} lg={7} xl={8}>

            <div className='total-amount'>
              <div className='title-logo'>
                <img src="https://defipay.oss-ap-southeast-1.aliyuncs.com/defipay_v_1.0/6f5acd0a3eab4f498285fed90b07898d.png" alt="" />
              </div>
              <div className='title'>今日交易总额</div>
              <div className='total-number'>
                <span className='amount'>12345</span>
                <span className='unit'>(usd)</span>
                <span className='pen'>共{ }笔</span>
              </div>
              <div className='total-sum'>
                <div className='sum-item'>
                  昨日交易总额 <span>0</span>
                </div>
                <div className='sum-item'>
                  近7日交易总额 <span>0</span>
                </div>
                <div className='sum-item'>
                  昨日交易笔数 <span>0</span>
                </div>
                <div className='sum-item'>
                  近7日交易笔数 <span>0</span>
                </div>
              </div>
            </div>
          </Col>
          <Col xs={3} sm={5} md={6} lg={7} xl={8}>
            <div className='accumulate-total'>
              <div className="title-logo">
                <img src="https://defipay.oss-ap-southeast-1.aliyuncs.com/defipay_v_1.0/2bc777a582d74c3890c6ab8bf4bf3f5e.png" />
              </div>
              <div className='title'>累计交易金额</div>
              <div className='total-number'>
                <span className='amount'>12345</span>
                <span className='unit'>(usd)</span>
              </div>
              <div className='title'>累计交易笔数</div>
              <div className='total-number'>
                <span className='amount'>12345</span>
                <span className='unit'>(usd)</span>
              </div>
            </div>
          </Col>
          <Col xs={3} sm={5} md={6} lg={7} xl={8}>
            <div className='service-total'>
              <div className="title-logo">
                <img src="https://defipay.oss-ap-southeast-1.aliyuncs.com/defipay_v_1.0/96988017f8fd491f8a35dfe592228386.png" />
              </div>
              <div className='title'>累计服务金额</div>
              <div className='total-number'>
                <span className='amount'>12345</span>
                <span className='unit'>笔</span>
              </div>
              <div className='title'>正在使用地址数量</div>
              <div className='total-number'>
                <span className='amount'>12345</span>
                <span className='unit'>条</span>
              </div>
            </div>
          </Col>
        </Row>
      </Card>
      <Card bordered={false}>
        <Tabs defaultActiveKey="1" tabBarGutter={50} items={items} onChange={onChange} />
      </Card>
      <Card title="币种交易看板" bordered={false}>
        <div className='currency-unit'>
          <p>接收</p>
          <p>单位: usd</p>
        </div>
        <div className='currency-receive'>
          <div className='receive-pay'>
            <ReceivePay />
          </div>
          <div className='receive-bar'>
            <ReceiveBar />
          </div>
        </div>
      </Card>
    </>
  )

}

export default Dashboard