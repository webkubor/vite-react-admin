import ErrorPage from "./error"
import RecordsPage from "./records"
import { Tabs } from 'antd';
import type { TabsProps } from 'antd';

const items: TabsProps['items'] = [
    {
      key: '1',
      label: `交易记录`,
      children: <RecordsPage/>,
    },
    {
      key: '2',
      label: `异常发送`,
      children: <ErrorPage/>,
    },
  ];

  const onChange = (key: string) => {
    console.log(key);
  };
  

function TradePage() {
    return (
        <Tabs defaultActiveKey="1" tabBarGutter={50} items={items} onChange={onChange} />
    )
}




export default TradePage