import { Button, Popconfirm, Input, Space } from "antd";
import { getUTCTime } from "@/hooks/useTime";
import { stateDom, TooltipeDom, RejectDom, TitleLegendDom, CurrencyDom } from "@/utils/renders";

const stateEnum: { [key: number]: { text: string, color: string } } = {
    0: {
        text: "审核通过",
        color: "success",
    },
    1: {
        text: "审核拒绝",
        color: "error",
    },
    2: {
        text: "商户审核通过（待运营审核）",
        color: "warning",
    },
    3: {
        text: "商户待审核",
        color: "default",
    },
    4: {
        text: "运营待审核",
        color: "default",
    },
}
export const createColumns = ({onTouch}:any) => [
    {
        title: '创建时间',
        dataIndex: 'cretaeTime',
        key: 'cretaeTime',
        show: true,
        fixed: true,
        width: 200,
        // render: (text: number) => {
        //     return getUTCTime(text * 1000)
        // }
    },
    {
        title: () => {
            return TitleLegendDom("支付流水号", ["提示文案提示文案提示文案提示文案提示文案提示文案提示文案提示文案提示文案提示文案提示文案提示文案提示文"])
        },
        dataIndex: 'id',
        key: 'id',
        show: true,
        width: 150,
        render: (text: string) => {
            return TooltipeDom(text, 150)
        }
    },
    {
        title: '商户订单号',
        dataIndex: 'acount',
        key: 'acount',
        show: true,
        width: 150,
        render: (text: string) => {
            return TooltipeDom(text, 150)
        }
    },
    {
        title: '商户名称',
        dataIndex: 'name',
        key: 'name',
        show: true,
        width: 100,
        render: (text: string) => {
            return TooltipeDom(text)
        }
    },
    {
        title: '商户编号',
        dataIndex: 'roleName',
        key: 'roleName',
        show: true,
        width: 200,
        render: (text: string) => {
            return TooltipeDom(text, 200)
        }
    },
    {
        title: '钱包',
        dataIndex: 'roleName',
        key: 'roleName',
        show: true,
        width: 100,

    },
    {
        title: '支付币种',
        dataIndex: 'roleName',
        key: 'roleName',
        show: true,
        width: 150,
        render: (text: string, record: any) => {
            return CurrencyDom(text, record.logo)
        }

    },
    {
        title: '支付数量',
        dataIndex: 'roleName',
        key: 'roleName',
        show: true,
        width: 100,
        render: (text: string) => {
            return TooltipeDom(text)
        }

    },
    {
        title: () => TooltipeDom('商户审核人'),
        dataIndex: 'roleName',
        key: 'roleName',
        show: true,
        width: 100,
        render: (text: string) => {
            return TooltipeDom(text)
        }
    },
    {
        title: '商户审核时间',
        dataIndex: 'roleName',
        key: 'roleName',
        show: true,
        width: 200,

    },
    {
        title: () => TooltipeDom('运营审核人'),
        dataIndex: 'roleName',
        key: 'roleName',
        show: true,
        width: 100,
        render: (text: string) => {
            return TooltipeDom(text)
        }
    },
    {
        title: '运营审核时间',
        dataIndex: 'roleName',
        key: 'roleName',
        show: true,
        width: 200,

    },
    {
        title: '审核状态',
        dataIndex: 'status',
        key: 'status',
        show: true,
        width: 100,
        render: (text: number) => {
            return stateDom(stateEnum[text]?.text, stateEnum[text]?.color)
        }
    },
    {
        title: '异常原因',
        dataIndex: 'roleName',
        key: 'roleName',
        show: true,
        width: 100,
        render: (text: string) => {
            return TooltipeDom(text)
        }

    },
    {
        title: '审核备注',
        dataIndex: 'roleName',
        key: 'roleName',
        show: true,
        width: 100,

        render: (text: string) => {
            return TooltipeDom(text)
        }
    },
    {
        title: '操作',
        dataIndex: 'action',
        key: 'action',
        fixed: "right",
        show: true,
        width: 210,
        render: () => {
            return <>
                <div style={{ display: "flex",justifyContent:"start" }}>
                    <Button type="link">重试</Button>
                    <Popconfirm
                        title="确定审核通过该笔发送？"
                        okText="确定"
                        cancelText="取消"
                        onConfirm={onTouch}
                    >
                        <Button type="link">通过</Button>
                    </Popconfirm>
                    <RejectDom okCallBack={okCallBack} />
                </div>
            </>
        }
    },
];


function okCallBack(rejectValue: string, setRejectValue: Function) {
    console.log(rejectValue);
    setRejectValue("")
}