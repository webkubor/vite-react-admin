import type { FilterValue, SorterResult } from 'antd/es/table/interface';
import type { ColumnsType, TablePaginationConfig } from 'antd/es/table';
import type { RangePickerProps } from 'antd/es/date-picker';
import type { InputProps } from 'antd/es/input';
import type { SelectProps } from 'antd/es/select';

export interface DataType {
    cretaeTime: number;
    acount: string;
    id: string;
    name: string;
    roleName: string;
    status: number;
}


export interface TableParams {
    pagination?: TablePaginationConfig;
    sortField?: string;
    sortOrder?: string;
    filters?: Record<string, FilterValue>;
}

export interface SearchParams {
    time: RangePickerProps["value"];
    condition: SelectProps["value"];
    conditionValue: InputProps["value"];
    state: SelectProps["value"];
    abnormal: SelectProps["value"];
}
