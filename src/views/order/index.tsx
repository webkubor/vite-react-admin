import { Button, Table, Space, Popover, Collapse, DatePicker, Col, Row, Input, Select, Modal } from "antd";
import { SettingOutlined } from '@ant-design/icons';
import { useState, useEffect } from "react";
import type { TablePaginationConfig } from 'antd/es/table';
import type { FilterValue, SorterResult } from 'antd/es/table/interface';
import { createColumns } from "./columns";
import ColumnsCheck from "@/components/ColumnsCheck";
import dayjs from 'dayjs';
import AutoRefresh from "@/components/AutoRefresh";
import GoogleCode from '@/components/GoogleCode';
import * as Interface from "./interface";
import { useTime } from "@/hooks/useTime";
import _ from "lodash"
import { tableWidth } from "@/utils/compute"
const { Panel } = Collapse;
const { RangePicker } = DatePicker;
const { Option } = Select;
const { timeZoneGetTime } = useTime();

const dataSource = [
    {
        cretaeTime: 1675388152,
        acount: "123456",
        id: '1',
        name: '胡彦斌',
        roleName: "系统管理员",
        status: 0,
        logo: "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
    },
    {
        cretaeTime: 1675388152,
        acount: "123456",
        id: '2',
        name: '胡彦斌',
        roleName: "系统管理员",
        status: 1,
    },
    {
        cretaeTime: 1675388152,
        acount: "123456",
        id: '3',
        name: '胡彦斌',
        roleName: "系统管理员",
        status: 2,
    },
    {
        cretaeTime: 1675388152,
        acount: "123456",
        id: '4',
        name: '胡彦斌',
        roleName: "系统管理员",
        status: 3,
    },
    {
        cretaeTime: 1675388152,
        acount: "123456",
        id: '5',
        name: '胡彦斌',
        roleName: "系统管理员",
        status: 4,
    },
];

function OrderPage() {
    const columnsSource = createColumns({ onTouch });
    let [columns, setColumns] = useState<object[]>(columnsSource);
    const tableScrollX = tableWidth(columnsSource);

    const [data, setData] = useState<Interface.DataType[]>();
    const [loading, setLoading] = useState(false);
    const [saveLoad, setSaveLoad] = useState<boolean>(false);
    const [isModalOpen, setModalShow] = useState<boolean>(false);
    const [tableParams, setTableParams] = useState<Interface.TableParams>({
        pagination: {
            current: 1,
            pageSize: 10,
        },
    });
    const [searchParams, setSearchParams] = useState<Interface.SearchParams>({
        time: null,
        condition: null,
        conditionValue: "",
        state: null,
        abnormal: null,
    })

    useEffect(() => {
        fetchData();
    }, [JSON.stringify(tableParams)]);

    const fetchData = () => {
        setLoading(true);
        setTimeout(() => {
            setData(dataSource);
            setTableParams({
                ...tableParams,
                pagination: {
                    ...tableParams.pagination,
                    total: 200,
                }
            })
            setLoading(false);
        }, 2000)
    }

    const onPageChange: any = (
        pagination: TablePaginationConfig,
        filters: Record<string, FilterValue>,
        sorter: SorterResult<Interface.DataType>,
    ) => {
        setTableParams({
            pagination,
            filters,
            ...sorter,
        });
        if (pagination.pageSize !== tableParams.pagination?.pageSize) {
            setData([]);
        }
    };

    function columnsCheckCb(data: any) {
        setColumns(data.filter((v: any) => {
            return v.show
        }))
    }

    function onSearch() {
        console.log(searchParams, "待审核发送搜索参数");
    }

    function onReset() {
        let resetData: any = {};
        Object.keys(searchParams).forEach((item) => {
            resetData[item] = null
        })
        setSearchParams(resetData)
    }

    function onTouch() {
        setModalShow(true)
    }

    function googleSave(code: string) {
        console.log("谷歌验证码");
        setSaveLoad(true)
        setTimeout(() => {
            setSaveLoad(false)
        }, 3000)

    }

    function handleSearchParams() {
        let startTime, endTime;
        if (searchParams.time) {
            startTime = timeZoneGetTime(searchParams.time[0] as any);
            endTime = timeZoneGetTime(searchParams.time[0] as any);
        }
        let params = {
            ...searchParams,
            startTime,
            endTime,
        }
        delete params.time;
        return _.pickBy(params);
    }

    const selectBefore = <Select
        filterOption allowClear
        style={{ width: "120px" }}
        placeholder="请选择条件"
        value={searchParams.condition}
        onChange={(v) => { setSearchParams({ ...searchParams, condition: v }) }} />;

    return <>
        <Collapse bordered={false}>
            <Panel header="搜索条件" key="search">
                <Row gutter={[{ xs: 8, sm: 12, md: 16, lg: 18, xl: 20, xxl: 24 }, 20]}>
                    <Col className="gutter-row" span={6}>
                        <RangePicker
                            allowClear
                            format="YYYY-MM-DD HH:mm:ss"
                            showTime={{
                                hideDisabledOptions: true,
                                defaultValue: [dayjs('00:00:00', 'HH:mm:ss'), dayjs('23:59:59', 'HH:mm:ss')],
                            }}
                            placeholder={["创建开始时间", "创建结束时间"]}
                            value={searchParams.time}
                            onChange={(v) => { setSearchParams({ ...searchParams, time: v }) }}
                        />
                    </Col>
                    <Col className="gutter-row" span={6}>
                        <Input
                            allowClear
                            addonBefore={selectBefore}
                            placeholder="请输入条件对应内容"
                            value={searchParams.conditionValue}
                            onChange={({ target }) => { setSearchParams({ ...searchParams, conditionValue: target.value }) }}
                        />
                    </Col>
                    <Col className="gutter-row" span={4}>
                        <Select
                            allowClear filterOption
                            placeholder="审核状态"
                            style={{ width: "100%" }}
                            value={searchParams.state}
                            onChange={(v) => { setSearchParams({ ...searchParams, state: v }) }}
                        />
                    </Col>
                    <Col className="gutter-row" span={4} >
                        <Select
                            allowClear filterOption
                            placeholder="异常原因"
                            style={{ width: "100%" }}
                            value={searchParams.abnormal}
                            onChange={(v) => { setSearchParams({ ...searchParams, abnormal: v }) }}
                        />
                    </Col>
                    <Col className="gutter-row" span={6} offset={18} >
                        <Space style={{ display: "flex", justifyContent: "end" }}>
                            <Button type="primary" onClick={onSearch}>搜索</Button>
                            <Button onClick={onReset}>重置</Button>
                        </Space>
                    </Col>
                </Row>
            </Panel>
        </Collapse>

        <Space style={{ display: "flex", justifyContent: "end" }}>
            <AutoRefresh onFetchList={fetchData} />
            <Popover content={<ColumnsCheck columnsSource={columnsSource} columnsCheckCb={columnsCheckCb} />} trigger="click" placement="left">
                <Button shape="circle" icon={<SettingOutlined />} />
            </Popover>
        </Space>

        <Table rowKey={(record) => record.id} dataSource={dataSource} columns={columns} pagination={tableParams.pagination}
            loading={loading}
            onChange={onPageChange}
            scroll={{ y: 600, x: tableScrollX }} />

        <Modal title="谷歌验证码" destroyOnClose footer={null} open={isModalOpen} width={550} onCancel={() => setModalShow(false)} >
            <GoogleCode codeLength={6} saveLoading={saveLoad} onSave={googleSave} onClose={() => { setModalShow(false); }} ></GoogleCode>
        </Modal>
    </>
}

export default OrderPage