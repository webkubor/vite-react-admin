import { Tabs } from 'antd';
import type { TabsProps } from 'antd';
import LogPage from './log';
import RolePage from './role';
import RiskPage from './risk';
import MemberPage from './member';

const onChange = (key: string) => {
    console.log(key);
  };
  
  const items: TabsProps['items'] = [
    {
      key: '1',
      label: `员工管理`,
      children: <MemberPage/>,
    },
    {
      key: '2',
      label: `角色管理`,
      children: <RolePage/>,
    },
    {
      key: '3',
      label: `操作日志`,
      children: <LogPage/>,
    },
    {
        key: '4',
        label: `风控配置`,
        children: <RiskPage/>,
      },
  ];
function Entry() {
    return (
        <Tabs defaultActiveKey="1" tabBarGutter={50} items={items} onChange={onChange} />
    )
  
}

export default Entry