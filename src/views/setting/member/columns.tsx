import { Button, Popconfirm } from "antd";
import { getUTCTime } from "@/hooks/useTime";
import { stateDom } from "@/utils/renders";

const stateEnum: { [key: number]: { text: string, color: string } } = {
    0: {
        text: "启用",
        color: "success",
    },
    1: {
        text: "取消",
        color: "processing",
    },
    2: {
        text: "停用",
        color: "error",
    },
    3: {
        text: "正在处理",
        color: "warning",
    },
    4: {
        text: "已处理",
        color: "default",
    }
}
export const createColumns = () => [
    {
        title: '创建时间',
        dataIndex: 'cretaeTime',
        key: 'cretaeTime',
        show: true,
        // render: (text: number) => {
        //     return getUTCTime(text * 1000)
        // }
    },
    {
        title: '员工ID',
        dataIndex: 'id',
        key: 'id',
        show: true,
    },
    {
        title: '账号',
        dataIndex: 'acount',
        key: 'acount',
        show: true,
    },
    {
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
        show: true,
    },
    {
        title: '角色名称',
        dataIndex: 'roleName',
        key: 'roleName',
        show: true,
    },
    {
        title: '状态',
        dataIndex: 'status',
        key: 'status',
        show: true,
        render: (text: number) => {
            return stateDom(stateEnum[text]?.text, stateEnum[text]?.color)
        }
    },
    {
        title: '操作',
        dataIndex: 'action',
        key: 'action',
        show: true,
        render: () => {
            return <>

                <Button type="link">修改信息</Button>
                <Button type="link">重置谷歌验证码</Button>
                <Popconfirm
                    title="是否确认停用或启用该账号？"
                    okText="确定"
                    cancelText="取消"
                >
                    <Button type="link">停用</Button>
                </Popconfirm>
                <Popconfirm
                    title="是否确认删除该账号？"
                    okText="确定"
                    cancelText="取消"
                >
                    <Button type="link">删除</Button>
                </Popconfirm>

            </>

        }
    },
];