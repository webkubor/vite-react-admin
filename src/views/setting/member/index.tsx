import { Button, Table, Space, Popover } from "antd";
import { SettingOutlined } from '@ant-design/icons';
import { useState, useEffect } from "react";
import type { ColumnsType, TablePaginationConfig } from 'antd/es/table';
import type { FilterValue, SorterResult } from 'antd/es/table/interface';
import { createColumns } from "./columns"
import ColumnsCheck from "@/components/ColumnsCheck"

const dataSource = [
    {
        cretaeTime: 1675388152,
        acount: "123456",
        id: '1',
        name: '胡彦斌',
        roleName: "系统管理员",
        status: 0,
    },
    {
        cretaeTime: 1675388152,
        acount: "123456",
        id: '2',
        name: '胡彦斌',
        roleName: "系统管理员",
        status: 1,
    },
    {
        cretaeTime: 1675388152,
        acount: "123456",
        id: '3',
        name: '胡彦斌',
        roleName: "系统管理员",
        status: 2,
    },
    {
        cretaeTime: 1675388152,
        acount: "123456",
        id: '4',
        name: '胡彦斌',
        roleName: "系统管理员",
        status: 3,
    },
    {
        cretaeTime: 1675388152,
        acount: "123456",
        id: '5',
        name: '胡彦斌',
        roleName: "系统管理员",
        status: 4,
    },
];

interface DataType {
    cretaeTime: number;
    acount: string;
    id: string;
    name: string;
    roleName: string;
    status: number;
}


interface TableParams {
    pagination?: TablePaginationConfig;
    sortField?: string;
    sortOrder?: string;
    filters?: Record<string, FilterValue>;
}

function MemberPage() {
    const columnsSource = createColumns();
    let [columns, setColumns] = useState<object[]>(columnsSource);

    const [data, setData] = useState<DataType[]>();
    const [loading, setLoading] = useState(false);
    const [tableParams, setTableParams] = useState<TableParams>({
        pagination: {
            current: 1,
            pageSize: 10,
        },
    });



    useEffect(() => {
        fetchData();
    }, [JSON.stringify(tableParams)]);

    const fetchData = () => {
        setLoading(true);
        setTimeout(() => {
            setData(dataSource);
            setTableParams({
                ...tableParams,
                pagination: {
                    ...tableParams.pagination,
                    total: 200,
                }
            })
            setLoading(false);
        }, 2000)
    }

    const handleTableChange = (
        pagination: TablePaginationConfig,
        filters: Record<string, FilterValue>,
        sorter: SorterResult<DataType>,
    ) => {
        console.log('%c%s', 'color: #00e600', '点击了分页器');
        setTableParams({
            pagination,
            filters,
            ...sorter,
        });

        // `dataSource` is useless since `pageSize` changed
        if (pagination.pageSize !== tableParams.pagination?.pageSize) {
            setData([]);
        }
    };

    function columnsCheckCb(data: any) {
        setColumns(data.filter((v: any) => {
            return v.show
        }))
    }

    return (
        <div>
            <Space style={{ display: "flex", justifyContent: "end" }}>
                <Popover content={<ColumnsCheck columnsSource={columnsSource} columnsCheckCb={columnsCheckCb} />} trigger="click" placement="left">
                    <Button shape="circle" icon={<SettingOutlined />} />
                </Popover>
            </Space>

            <Table rowKey={(record) => record.id} dataSource={dataSource} columns={columns} pagination={tableParams.pagination}
                loading={loading}
                showSizeChanger
                showQuickJumper
                onChange={handleTableChange} />
        </div>
    )
}

export default MemberPage