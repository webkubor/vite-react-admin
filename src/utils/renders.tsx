import { Button, Space, Badge, Tag, Tooltip, Popconfirm, Input, Popover, Avatar } from 'antd';
import { useState } from "react";
import { QuestionCircleOutlined } from '@ant-design/icons';
import "../style/render.less";

/**
 * @description: 列表状态渲染
 * @param : 入参状态文案和状态color标识
 * @return {*}
 */
export function stateDom(stateText: string, state: string) {
    return <Tag color={state}>{TooltipeDom(stateText, 60)}</Tag>
}

/**
 * @description: 文案气泡
 * @param : 入参所需展示文案
 * @return {*}
 */
export function TooltipeDom(text: string, width: number = 100) {
    return <Tooltip title={text}>
        <div className='tooltip_ellipsis_text' style={{ width: `${width}px` }}>{text}</div>
    </Tooltip>
}

/**
 * @description: 拒绝二次确认附带原因输入
 * @param : 入参 一个回调函数
 * @return {*}
 */
export function RejectDom(props: { okCallBack: Function }) {
    const { okCallBack } = props
    let [rejectValue, setRejectValue] = useState("")
    return <Popconfirm
        title="审核备注"
        okText="确定"
        cancelText="取消"
        onConfirm={() => { okCallBack(rejectValue, setRejectValue) }}
        onCancel={() => { setRejectValue("") }}
        description={() => {
            return <Input value={rejectValue} onChange={({ target }) => { setRejectValue(target.value) }} />;
        }}
    >
        <Button type="link">拒绝</Button>
    </Popconfirm>
}

/**
 * @description: 表格title说明
 * @param : 入参 
 * @return {*}
 */
export function TitleLegendDom(title: string, contentData: unknown[]) {
    const content = <>
        {contentData.map((item, index) => {
            return <div className='table_title_legend_text' key={"title_legend" + index}>
                {contentData.length > 1 ? `${index + 1}. ${item}` : item}
            </div>
        })}
    </>
    return <Popover content={content}>
        {title} <QuestionCircleOutlined />
    </Popover>
}

/**
 * @description: 币种组件
 * @param : 入参 币种名称，币种logo 
 * @return {*}
 */
export function CurrencyDom(name: string, logo: string) {
    return <>
        <Space>
            {logo && <Avatar size={24} src={logo} />}
            <span>{name}</span>
        </Space>
    </>
}