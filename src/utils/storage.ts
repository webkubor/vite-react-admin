



export function  getTokenStorage():string | null {
    return  localStorage.getItem(import.meta.env.VITE_BASE_PRE_NAME +  "_manager_token") 
}



export function getLangStorage():string| null {
    return localStorage.getItem( import.meta.env.VITE_BASE_PRE_NAME + "_manager_lang")
}