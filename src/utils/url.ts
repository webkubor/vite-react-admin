
/**
 * @description: 在新标签页打开网页
 * @param {*} url 需要打开的url
 * @param {*} id 防止重复位置打开
 * @return {*}
 */
 export function openUrl(url:string, id = "default") {
    var a = document.createElement("a");
    a.setAttribute("href", url);
    a.setAttribute("target", "_blank");
    a.setAttribute("id", id);
    // 防止反复添加
    if (!document.getElementById(id)) {
      document.body.appendChild(a);
    }
    a.click();
  }


  /**
   * @description: 解析url中的携带参数
   * @param {*} val
   * @return {*}
   */
  export function parseQuery(val) {
    var params = {},
      seg = val.replace(/^\?/, '').split('&'),
      len = seg.length,
      p;
    for (var i = 0; i < len; i++) {
      if (seg[i]) {
        p = seg[i].split('=');
        params[p[0]] = p[1];
      }
    }
    return params;
  }
