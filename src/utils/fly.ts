import fly from "flyio";
import _ from "lodash";
import {getTokenStorage} from "@/utils/storage"

fly.config.baseURL =
  import.meta.env.VITE_BASE_URL +
  import.meta.env.VITE_BASE_SERVER +
  import.meta.env.VITE_BASE_API_VERSION;

function reLogin() {
}

/**
 * @description: 处理错误抛出
 * @param {*} function
 * @return {*}
 */
 const handleError = _.debounce(function (message) {
  window.$message?.error('error: ' + message, { duration: 3000 });
}, 2500);


fly.interceptors.request.use((request) => {
  request.headers.Accept = "application/json";
  if (getTokenStorage()) {
    request.headers["token"] = getTokenStorage();
  }
  return request;
});

// 添加响应拦截器，响应拦截器会在then/catch处理之前执行
fly.interceptors.response.use(
  (response) => {
    const { status, request } = response;
    let res = response.data;

    if (status === 200) {
      if (request.responseType === "blob") {
        return Promise.resolve(res);
      }
      if (res.code === 200) {
        return Promise.resolve(res.data);
      }
    }
    if (res.msg) {
      if (res.code === 500400) {
        reLogin()
      }
      handleError(res.msg);
    }
    return Promise.reject(res);
  },

  (err) => {
    if (err.message) {
      handleError(err.message);
      return Promise.reject(err.message);
    }
    return Promise.reject(err);
  }
);

export default fly;




