import _ from "lodash";

/**
 * @name: 处理金额
 * @param {*} num
 * @param {*} decimal
 * @return {*}
 */
 export function formatAmount(num, decimal = 2) {
  if(!num) return num
  let result = _.floor(num * 1, decimal)
  return amountSplitThousand(result);
}


export function dealDecimals(num, decimal) {
  const magnification=Math.pow(10,decimal)
  return Math.floor(num * magnification) / magnification
}

/**
 * @name: 千位分隔
 * @param {*} num
 * @return {*}
 */
 function amountSplitThousand(num) {
  let parts = num.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts.join(".");
}


/**
 * @description: 长文本省略中间部分
 * @param {string} str 文本内容
 * @param {number} start 保留文本头部长度
 * @param {number} end 保留文本尾部长度
 * @return {string}
 */
export function ellipsisStr(str, start = 6, end = 4) {
  if (typeof str === "string") {
    return str.slice(0, start) + "..." + str.slice(-end);
  }
  return str;
}

