
/**
 * @description: 生成一个随机数字(可指定最大值)
 * @param {*} maxNumber
 * @return {*}
 */
export function getNumber(maxNumber=1000) {
    return Math.floor(Math.random()*maxNumber)
}

/**
 * @description: 获取时间戳毫秒
 * @param {*}
 * @return {*}
 */
 export function getTimeStemp() {
    return new Date().getTime()
}

