/**
 * 表格下载
 */
export function downExcel(res, name) {
    let current = new Date()
    let today =
        current.getFullYear() +
        (current.getMonth() + 1).toString().padStart(2, '0') +
        current.getDate().toString().padStart(2, '0') +
        current.getHours().toString().padStart(2, '0') +
        current.getMinutes().toString().padStart(2, '0') +
        current.getSeconds().toString().padStart(2, '0') 

    const blob = new Blob([res]); // 把得到的结果用流对象转一下
    var a = document.createElement("a"); //创建一个标签

    a.href = URL.createObjectURL(blob); // 将流文件写入a标签的href属性值

    a.download = name + "_" + today + ".xls"; //设置文件名

    a.style.display = "none"; // 障眼法藏起来a标签

    document.body.appendChild(a); // 将a标签追加到文档对象中

    a.click(); // 模拟点击了a标签，会触发a标签的href的读取，浏览器就会自动下载了

    a.remove(); // 一次性的，用完就删除a标签
}