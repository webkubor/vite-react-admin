import BigNumber from 'bignumber.js'


/**
 * @description: 加
 * @param {string} num1
 * @param {string} num2
 * @return {*}
 */
export function add(num1:string, num2:string) {
  return new BigNumber(num1).plus(num2);
}


/**
 * @description: 减
 * @param {string} num1
 * @param {string} num2
 * @return {*}
 */
export function subtract(num1:string, num2:string)  {
  return new BigNumber(num1).minus(num2);
}


/**
 * @description: 成
 * @param {string} num1
 * @param {string} num2
 * @return {*}
 */
export function multiply(num1:string, num2:string)  {
  return new BigNumber(num1).times(num2);
}


/**
 * @description: 除
 * @param {string} num1
 * @param {string} num2
 * @return {*}
 */
export  function divide(num1:string, num2:string) {
  return new BigNumber(num1).dividedBy(num2);
}


/**
 * @description: 按照精度截取，并舍去以0结尾的部分
 * @param {string} num
 * @param {*} digits
 * @return {*}
 */
export function truncate(num:string, digits=2) {
    const result = new BigNumber(num).toFormat(digits);
    return result.endsWith('.00') ? result.slice(0, -3) : result.replace(/\.?0+$/, '');
  }