import _ from "lodash"

export function tableWidth(columns: object[]) {
    return _.reduce(columns, (sum: any, n: any) => {
        return sum + n.width
    }, 0)
}