import { createContext, useReducer, useContext } from "react";
import {getTimeZone} from "@/hooks/useTime"
import { IContext, IState } from "./user.interface"

export const initialState = {
    active: false,
    timeArea: getTimeZone(),
}

// 变量定义
export const UserContext = createContext<IContext>({
    state: initialState,
    dispatch: () => { }
})


export const reducer = (preState: IState, action: {
    type: string;
    payload?: Partial<IState>;
}) => {
    const { type, payload } = action;

    switch (type) {
        default:
            return preState;
        case "switch_active":
            return { ...preState, active: !preState.active };
        case 'switch_time_zone':
            return {
                ...preState,
                timeArea: payload,
            };

    }

};


// 共享配置
export const UserProvider = (props: { children: JSX.Element }) => {
    const [state, dispatch] = useReducer(reducer, initialState)
    return (
        <UserContext.Provider value={{ state, dispatch }}>
            {props.children}
        </UserContext.Provider>
    )
}



export const useUserContext = () => {
    return useContext(UserContext);
};