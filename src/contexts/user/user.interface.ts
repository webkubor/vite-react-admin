
import { Dispatch } from "react";

export interface IState {
    active: Boolean;
    timeArea: string;
}
export interface IContext {
    state: IState;
    dispatch: Dispatch<{
        type: string;
        payload?: Partial<IState>;
    }>;
}