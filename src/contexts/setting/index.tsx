import { createContext, useReducer, useContext } from "react";
import { IContext, IState } from "./setting.interface"

export const initialState = {
    fullscreen: false,  // 是否全屏
    collapsed: false, //  是否收缩菜单栏目
}


/**
 * @description: 初始化变量定义
 * @return {*}
 */
export const SettingContext = createContext<IContext>({
    settingState: initialState,
    setGlobalConfig: () => { }
})


export const reducer = (preState: IState, action: {
    type: string;
    payload?: Partial<IState>;
}) => {
    const { type, payload } = action;

    switch (type) {
        default:
            return preState;
        case "toggle_fullscreen":
            return { ...preState, locale: payload };
        case 'toggle_collapsed':
            console.log('%c%s', 'color: #00a3cc', preState.collapsed);
            return {
                ...preState,
                collapsed: !preState.collapsed
            };

    }

};



// 共享配置
export const SettingProvider = (props: { children: JSX.Element }) => {
    const [settingState, setGlobalConfig] = useReducer(reducer, initialState)
    return (
        <SettingContext.Provider value={{ settingState, setGlobalConfig }}>
            {props.children}
        </SettingContext.Provider>
    )
}



export const useSettingContext = () => {
    return useContext(SettingContext);
};