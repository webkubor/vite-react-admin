import { Dispatch } from "react";

export interface IState {
    fullscreen: boolean;
    collapsed:boolean;
}

export interface IContext {
    settingState: IState;
    setGlobalConfig: Dispatch<{
        type: string;
        payload?: Partial<IState>;
    }>;
}