import React from 'react'
import { message } from 'antd';
import { ConfigProvider } from 'antd';
import { StyleProvider } from '@ant-design/cssinjs';
import { UserProvider } from "@/contexts/user"; // 提供用户相关的上下文数据
import { SettingProvider } from '@/contexts/setting'; // 提供设置相关的上下文数据
import { HashRouter } from 'react-router-dom';
import GetRouters from './router';
import theme from "./theme/index"
import BeforeRouter from './router/beforeRoute';

const App: React.FC = () => {
     // 使用 antd 的 message 模块获取消息 API 和上下文占位符
  const [messageApi, contextHolder] = message.useMessage();
  // 将消息 API 挂载到全局的 window 对象上
  window.$message = messageApi
  document.title = import.meta.env.VITE_BASE_PRE_NAME + '-Manager'
  return (
    <StyleProvider hashPriority="high">
      <ConfigProvider theme={theme}>
        {contextHolder}
        <SettingProvider>
          <UserProvider>
            <HashRouter>
              <BeforeRouter>
                <GetRouters />
              </BeforeRouter>
            </HashRouter>
          </UserProvider>
        </SettingProvider>
      </ConfigProvider>
    </StyleProvider>
  )
}

export default App
