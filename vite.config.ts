import { defineConfig } from 'vite'
import { fileURLToPath, URL } from 'url'
import react from '@vitejs/plugin-react'
import svgr from 'vite-plugin-svgr' 


export default defineConfig({
  base: process.env.NODE_ENV === "development" ? "/" : "./",
  resolve: {
    extensions: [".tsx",".ts", ".jsx", ".js", ".jsx", ".json"],
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  plugins: [ react(),   svgr()],
  build: {
    reportCompressedSize: false,
  },
  css: {
    preprocessorOptions:{
      less: {
        // 支持内联 JavaScript
        javascriptEnabled: true,
        charset: false,
        additionalData: '@import "./src/style/index.less"; '
      }
    }
  }
})
